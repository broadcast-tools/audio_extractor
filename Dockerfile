FROM registry.gitlab.com/broadcast-tools/docker-multimedia

ARG GIT_HASH
ENV GIT_HASH=${GIT_HASH}

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV VIRTUAL_ENV /app/venv

RUN apk add --no-cache --update gcc g++ make python3 python3-dev py3-pip py3-wheel

RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

WORKDIR /app

COPY requirements.txt /app/
RUN pip install -r requirements.txt --no-cache

COPY main.py /app/

EXPOSE 8000

CMD ["uvicorn", "--workers", "4", "--host", "0.0.0.0", "main:app"]
