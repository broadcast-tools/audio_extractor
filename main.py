import os
import subprocess
import logging
from uuid import uuid4
from pathlib import Path
from tempfile import gettempdir
from enum import Enum
from collections import namedtuple

import aiofiles
from fastapi import FastAPI, UploadFile, Request
from fastapi.responses import StreamingResponse, PlainTextResponse, Response, FileResponse
from fastapi.exceptions import HTTPException
from fastapi.middleware.cors import CORSMiddleware

# https://trac.ffmpeg.org/wiki/Seeking
# https://ffmpeg.org/ffmpeg-utils.html#time-duration-syntax
# HOURS:MM:SS.MILLISECONDS
# [-][HH:]MM:SS[.m...]
# [-]S+[.m...][s|ms|us]
# ToDo: Add support for different Channel
# ToDo: Add support for Mixdown

logging.basicConfig(
    level=os.environ.get("LOGLEVEL", "INFO"),
    format='%(asctime)s %(message)s'
)

GIT_HASH = os.environ.get("GIT_HASH", "dev")
logging.info(f"Git Hash: {GIT_HASH}")

SENTRY_DSN = os.environ.get("SENTRY_DSN")
SENTRY_RELEASE = os.environ.get("SENTRY_RELEASE", GIT_HASH)
SENTRY_ENVIRONMENT = os.environ.get("SENTRY_ENVIRONMENT", "dev")
SENTRY_CA_CERTS = os.environ.get("SENTRY_CA_CERTS")

if SENTRY_DSN is not None:
    import sentry_sdk
    from sentry_sdk.integrations.starlette import StarletteIntegration
    from sentry_sdk.integrations.fastapi import FastApiIntegration

    sentry_params = {
        "dsn": SENTRY_DSN,
        "integrations": [
            StarletteIntegration(),
            FastApiIntegration(),
        ],
        "release": SENTRY_RELEASE,
        "environment": SENTRY_ENVIRONMENT,
        "traces_sample_rate": 1.0,
    }

    if SENTRY_CA_CERTS is not None:
        sentry_params["ca_certs"] = SENTRY_CA_CERTS

    sentry_sdk.init(**sentry_params)

BASE_PATH = os.environ.get("BASE_PATH", gettempdir())
FFMPEG_BIN = "/usr/bin/ffmpeg"
DEFAULT_CHUNK_SIZE = 1024 * 1024 * 4  # 4 megabytes

if not Path(BASE_PATH).exists():
    logging.error(f"{BASE_PATH} does not exists")
    exit(1)

if not Path(FFMPEG_BIN).exists():
    logging.error(f"{FFMPEG_BIN} does not exists")
    exit(1)

CodecParams = namedtuple("CodecSettings", ["codec", "media_type", "transport", "suffix"])


class CodecSettings(CodecParams, Enum):
    AAC = CodecParams(codec="aac", media_type="audio/aac", transport="adts", suffix="aac")
    MP3 = CodecParams(codec="mp3", media_type="audio/mp3", transport="mp3", suffix="mp3")
    WAV = CodecParams(codec="pcm_s16le", media_type="audio/wav", transport="wav", suffix="wav")


class Codec(str, Enum):
    AAC = "AAC"
    MP3 = "MP3"
    WAV = "WAV"


app = FastAPI(
    title="Audio Extractor",
    description="Return audio tracks from multimedia files",
    version=GIT_HASH,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get(path="/", include_in_schema=False)
async def get_root():
    return PlainTextResponse(f"{app.title} - {app.description}")


@app.post("/upload")
async def upload(request: Request, file: UploadFile):
    logging.info(f"File: {file.filename}")

    uuid = f"{uuid4()}"
    suffix = Path(file.filename).suffix
    upload_file = Path(BASE_PATH) / Path(f"{uuid}{suffix}")

    async with aiofiles.open(upload_file, mode="wb") as f:
        while chunk := await file.read(DEFAULT_CHUNK_SIZE):
            await f.write(chunk)

    url_audio = request.url_for("get_audio", filename=f"{uuid}{suffix}")

    return {"url_audio": url_audio}


@app.head(path="/audio/{filename}", include_in_schema=False)
def head_audio(filename: str):
    logging.info(f"Base Path: {BASE_PATH}")
    logging.info(f"Filename: {filename}")

    base_path = Path(BASE_PATH) if BASE_PATH is not None and Path(BASE_PATH).exists() else None
    filename_path = Path(filename)

    if base_path is None:
        logging.error(f"{BASE_PATH} does not exists")
        raise HTTPException(status_code=500, detail=f"{BASE_PATH} does not exists")

    file_path = Path(base_path / filename_path)
    file = file_path if file_path.exists() else None

    if file is None:
        logging.error(f"{filename} does not exists")
        raise HTTPException(status_code=404, detail=f"{filename} does not exists")

    return Response(
        media_type="audio/aac",
        headers={"Content-Disposition": "attachment"}
    )


@app.get(path="/audio/{filename}")
async def get_audio(filename: str, codec: Codec = Codec.AAC, ss: str = None, to: str = None):
    logging.info(f"Base Path: {BASE_PATH}")
    logging.info(f"Filename: {filename}")
    logging.info(f"Codec: {codec}")

    codec_settings = CodecSettings[codec]
    logging.info(f"Codec Settings: {codec_settings}")

    base_path = Path(BASE_PATH) if BASE_PATH is not None and Path(BASE_PATH).exists() else None
    filename_path = Path(filename)

    if base_path is None:
        logging.error(f"{BASE_PATH} does not exists")
        raise HTTPException(status_code=500, detail=f"{BASE_PATH} does not exists")

    file_path = Path(base_path / filename_path)
    file = file_path if file_path.exists() else None

    if file is None:
        logging.error(f"{filename} does not exists")
        raise HTTPException(status_code=404, detail=f"{filename} does not exists")

    args = [
        FFMPEG_BIN,
        "-hide_banner",
        "-loglevel", "quiet",
    ]

    if ss is not None:
        args.extend(
            ["-ss", ss],
        )

    args.extend(
        ["-i", f"{file}"],
    )

    if to is not None:
        args.extend(
            ["-to", to, "-copyts"],
        )

    args.extend([
        "-vn",
        "-acodec", f"{codec_settings.codec}",
        "-f", f"{codec_settings.transport}",
        "pipe:stdout",
    ])

    process = subprocess.Popen(args, stdout=subprocess.PIPE)
    if isinstance(process.returncode, int):
        raise HTTPException(status_code=500, detail=f"FFMPEG process failed")

    def generate():
        buffer = [process.stdout.read(1024)]

        while True:
            if len(buffer) > 0:
                yield buffer.pop(0)

            buffer.append(process.stdout.read(1024))

            process.poll()
            if isinstance(process.returncode, int):
                break

    uuid = f"{uuid4()}"

    return StreamingResponse(
        generate(),
        media_type=f"{codec_settings.media_type}",
        headers={"Content-Disposition": f"attachment;filename={uuid}.{codec_settings.suffix}"}
    )


@app.get(path="/audio")
async def get_audio_from_url(url: str, codec: Codec = Codec.AAC, ss: str = None, to: str = None):
    logging.info(f"URL: {url}")
    logging.info(f"Codec: {codec}")

    codec_settings = CodecSettings[codec]
    logging.info(f"Codec Settings: {codec_settings}")

    args = [
        FFMPEG_BIN,
        "-hide_banner",
        "-loglevel", "quiet",
        "-icy", "0",
        "-multiple_requests", "1",
        "-reconnect", "1",
        "-reconnect_streamed", "1",
    ]

    if ss is not None:
        args.extend(
            ["-ss", ss],
        )

    args.extend(
        ["-i", f"{url}",],
    )

    if to is not None:
        args.extend(
            ["-to", to, "-copyts"],
        )

    args.extend([
        "-vn",
        "-acodec", f"{codec_settings.codec}",
        "-f", f"{codec_settings.transport}",
        "pipe:stdout",
    ])

    process = subprocess.Popen(args, stdout=subprocess.PIPE)
    if isinstance(process.returncode, int):
        raise HTTPException(status_code=500, detail=f"FFMPEG process failed")

    def generate():
        buffer = [process.stdout.read(1024)]

        while True:
            if len(buffer) > 0:
                yield buffer.pop(0)

            buffer.append(process.stdout.read(1024))

            process.poll()
            if isinstance(process.returncode, int):
                break

    uuid = f"{uuid4()}"

    return StreamingResponse(
        generate(),
        media_type=f"{codec_settings.media_type}",
        headers={"Content-Disposition": f"attachment;filename={uuid}.{codec_settings.suffix}"}
    )


@app.get(path="/file/{filename}")
def get_file(filename: str):
    logging.info(f"Base Path: {BASE_PATH}")
    logging.info(f"Filename: {filename}")

    base_path = Path(BASE_PATH) if BASE_PATH is not None and Path(BASE_PATH).exists() else None
    filename_path = Path(filename)

    if base_path is None:
        logging.error(f"{BASE_PATH} does not exists")
        raise HTTPException(status_code=500, detail=f"{BASE_PATH} does not exists")

    file_path = Path(base_path / filename_path)
    file = file_path if file_path.exists() else None

    if file is None:
        logging.error(f"{filename} does not exists")
        raise HTTPException(status_code=404, detail=f"{filename} does not exists")

    return FileResponse(
        file,
        headers={"Content-Disposition": f"attachment;filename={filename}"}
    )


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000)
